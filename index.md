# How to build *yours* website

---

# **_Why?_** 

- Do you love to write things?
- You want to save your content: tips, stuff, code snippets,..?
- Share your knowledge with the world?
- PR yourself - Lets others know who you are - Why not?
- Just want to get fun? 😂

--- 

And? 

---

No coding skills? 🤔  

![No Coding skills](/code.png)

--- 
_/* No coding skills? */_

```javascript
for (let yourAge = 0; yourAge < 100; yourAge++ ) {
    let codingSkill = "";
    if (codingSkill == "Good") {
        console.log(🙌);
    } else {
        console.log("Go to die! 💀");
    }
}   
```

---

Build **your** blog/website *make easy!*

---

*Just Work*

``¯\_(ツ)_/¯``

    😎

---

# How to?

---

# Tools/Services:

- GitLab
- GitHub
- Now.sh
- ...

---

# How it works?

- Use git source as a hosting. (GitLab/GitHub)
- Deploy your source code to the server with single command/click (Now.sh)

---

# Features 

- Fast
- Custom domain (use your own domain)
- *100%* free
- No coding skills

---

 # A. Build your website with *GitLab/GitHub* pages:

*1.* Choose your framework/template or just write anything.

- GitLab pages example: https://gitlab.com/pages
- GitHub pages: https://pages.github.com/themes/
- Hugo templates: https://gohugo.io/showcase/
- Any template you love 😉 (somewhere on internet) 

*2.* Write

*3.* Commit/save.

---

# B. Build your website with *Now.sh*

*1.* Install *Now CLI* or *Desktop app*

*2.* Click to Deploy new update or just type *now*

*3.* Enjoy!

---

😎  ![](/Fun.gif) 😎

---

# The *next* presentation?

---

How-to-*Docker*

---

Thank you!
